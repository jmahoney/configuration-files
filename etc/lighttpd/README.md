I believe lighttpd.conf is just the stock configuration that is installed via `apt-get lighttpd`.

Specific local changes are in the conf-enabled directory. On Devuan and Debian, the way to add things to the conf-enabled directory is with the `lighty-enable-mod` command.

Regarding 90-javascript-alias.conf, I believe Devuan (by way of Debian) installs that by default. This link is informative: <https://sbaronda.com/2015/06/11/lighttpd-javascript-alias-issue/>.