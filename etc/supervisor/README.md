`supervisord.conf` is the stock file installed via `apt-get install supervisor`.

The last line is key, and is how `conf.d/fedwiki.conf` (the important part) gets loaded.