These are the commands I ran to set up my ultra-simple `ufw` configuration. Replace ---- with the SSH port number of the server.

    apt-get install ufw
    ufw status
    ufw enable
    ufw status
    ufw status verbose
    ufw allow ----/tcp
    ufw default reject incoming
    ufw allow 80/tcp
    ufw allow 443/tcp
    ufw status
    ufw status verbose

Read <http://www.chiark.greenend.org.uk/%7Epeterb/network/drop-vs-reject>, which convinced me to go with `ufw default reject incoming` instead of `ufw default drop incoming`.

Here are the results of `ufw status`:

    # ufw status
    Status: active
    
    To                         Action      From
    --                         ------      ----
    ----/tcp                   ALLOW       Anywhere
    80/tcp                     ALLOW       Anywhere
    443/tcp                    ALLOW       Anywhere
    ----/tcp (v6)              ALLOW       Anywhere (v6)
    80/tcp (v6)                ALLOW       Anywhere (v6)
    443/tcp (v6)               ALLOW       Anywhere (v6)

 Here are the results of `ufw status verbose`:

    # ufw status verbose
    Status: active
    Logging: on (low)
    Default: reject (incoming), allow (outgoing), disabled (routed)
    New profiles: skip
    
    To                         Action      From
    --                         ------      ----
    ----/tcp                   ALLOW IN    Anywhere
    80/tcp                     ALLOW IN    Anywhere
    443/tcp                    ALLOW IN    Anywhere                  
    ----/tcp (v6)              ALLOW IN    Anywhere (v6)
    80/tcp (v6)                ALLOW IN    Anywhere (v6)
    443/tcp (v6)               ALLOW IN    Anywhere (v6)